java -jar FailureDetector.jar "config/case1_topology.txt" "config/case1_jvm.txt" "config/case1_branch.txt"  &

sleep 1

java -jar BranchGUI.jar    G1 "config/case1_topology.txt" "config/case1_jvm.txt" "config/case1_branch.txt" &
java -jar BranchGUI.jar    G2 "config/case1_topology.txt" "config/case1_jvm.txt" "config/case1_branch.txt" &
java -jar BranchServer.jar S1 "config/case1_topology.txt" "config/case1_jvm.txt" "config/case1_branch.txt" &
java -jar BranchServer.jar S2 "config/case1_topology.txt" "config/case1_jvm.txt" "config/case1_branch.txt" &

java -jar BranchGUIDriver.jar "config/case1_script.txt" "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt"
