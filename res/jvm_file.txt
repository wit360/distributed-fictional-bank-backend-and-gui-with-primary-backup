# Type
# - FD     = special config for failure detector
# - GUI    = (2 digit branch code)_0
# - Server = (2 digit branch code)_(running number)

#---------------------------------------------
# Type      JVM      IP Address  Port
#---------------------------------------------
FD          FD       127.0.0.1   40999

GUI         G1       127.0.0.1   41001
GUI         G2       127.0.0.1   41002

SERVER 	    S1       127.0.0.1   47001
SERVER 	    S2       127.0.0.1   47002


exit

FD          FD       127.0.0.1   40999

GUI         G1       127.0.0.1   41001
GUI         G2       127.0.0.1   41002
GUI         G3       127.0.0.1   41003

SERVER 	    S1       127.0.0.1   47001
SERVER 	    S2       127.0.0.1   47002
SERVER 	    S3       127.0.0.1   47003
