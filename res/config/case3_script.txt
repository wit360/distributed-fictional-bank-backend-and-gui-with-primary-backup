# deposit 		[FRONT_JVM]  deposit  [Serial No] [Account No] [Amount]
# withdraw 		[FRONT_JVM]  withdraw [Serial No] [Account No] [Amount]
# query 		  [FRONT_JVM]  query    [Serial No] [Account No]
# transfer 		[FRONT_JVM]  transfer [Serial No] [Source Account No] [Destination Account No] [Amount]
# server down	[FD_JVM]     s-down   [FAILED_JVM]	
# server up 	[FD_JVM]     s-up     [FAILED_JVM]
# notify down	[FD_JVM]     n-down   [FAILED_JVM]	
# notify up 	[FD_JVM]     n-up     [FAILED_JVM]
# delay     	delay  [Interval in millisecond]
# exit        exit

# TEST CASE 3
# - (use the same configuration as test 2)
# - test client retransmission
# - 1. G1 normal deposit
# - 2. down S1 (primary) (without notifying)
# - 3. G1 normal deposit (waiting for S1's response)
# - 4. down-notify S1 (G1 retransmit to S2 and succeed)
# - 5. down S2 (primary) (without notifying)
# - 6. G1 normal deposit (waiting for S2's response)
# - 7. down-notify S2 (G1 gives up because there is no server left)

# configuration:
# 10 G1 S1 S2
# 20 G2 S2 S1
# --------------------------------------

delay  2000

# 1 --------------------------------------

G1 deposit   DG1001 10.00000 10000
delay

# 2 --------------------------------------

FD s-down    S1
delay 2000

# 3 --------------------------------------

G1 deposit   DG1002 10.00000 10000
delay

# 4 --------------------------------------

FD n-down    S1
delay

# 5 --------------------------------------

FD s-down    S2
delay

# 6 --------------------------------------

G1 deposit   DG1003 10.00000 10000
delay

# 7 --------------------------------------

FD n-down    S2
exit

