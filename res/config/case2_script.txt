# deposit 		[FRONT_JVM]  deposit  [Serial No] [Account No] [Amount]
# withdraw 		[FRONT_JVM]  withdraw [Serial No] [Account No] [Amount]
# query 		[FRONT_JVM]  query    [Serial No] [Account No]
# transfer 		[FRONT_JVM]  transfer [Serial No] [Source Account No] [Destination Account No] [Amount]
# server down	[FD_JVM]     s-down   [FAILED_JVM]	
# server up 	[FD_JVM]     s-up     [FAILED_JVM]
# notify down	[FD_JVM]     n-down   [FAILED_JVM]	
# notify up 	[FD_JVM]     n-up     [FAILED_JVM]
# delay     	delay  [Interval in millisecond]
# exit        exit

# TEST CASE 2
# 1. create an account on each branch
# 2. simulate failure on S3 then recover
# 3. simulate failure on S1 and S2, making S3 processes 3 branches
# 4. try circular transfer (10->20, 20->30, 30->10)
# 5. query 3 accounts

# configuration:
# 10 G1 S1 S2 S3
# 20 G2 S2 S3
# 30 G3 S3 S1
# --------------------------------------
delay  3000

# 1 --------------------------------------

G1 deposit  G1D001 10.00000 10000
G2 deposit  G2D001 20.00000 20000
G3 deposit  G3D001 30.00000 30000
delay

# 2 --------------------------------------

FD s-down   S3
delay
FD n-down   S3
delay

G1 deposit  G1D002 10.00000 1
G2 deposit  G2D002 20.00000 2
G3 deposit  G3D002 30.00000 3
delay

FD s-up     S3
delay
FD n-up     S3
delay

# 3 --------------------------------------

FD s-down   S1
delay
FD s-down   S2
delay
FD n-down   S1
delay
FD n-down   S2
delay

# --------------------------------------

G1 query     G1Q001 10.00000
G2 query     G2Q001 20.00000
G3 query     G3Q001 30.00000

delay 3000

# 4 --------------------------------------

G1 transfer  G1T001 10.00000 20.00001  9000
delay
G2 transfer  G2T001 20.00000 30.00001 18000
delay
G3 transfer  G3T001 30.00000 10.00001 27000
delay

# 5 --------------------------------------

G1 query     G1Q002 10.00001
G2 query     G2Q002 20.00001
G3 query     G3Q002 30.00001
# expected value
# 10.00000 = 1001   (10001-9000)
# 20.00000 = 2002   (20002-18000)
# 20.00000 = 3003   (30003-27000)
# 10.00001 = 9000
# 20.00001 = 18000
# 30.00001 = 27000

exit

