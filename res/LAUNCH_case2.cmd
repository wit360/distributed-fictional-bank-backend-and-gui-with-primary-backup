java -jar FailureDetector.jar "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &

sleep 1

java -jar BranchGUI.jar    G1 "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &
java -jar BranchGUI.jar    G2 "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &
java -jar BranchGUI.jar    G3 "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &
java -jar BranchServer.jar S1 "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &
java -jar BranchServer.jar S2 "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &
java -jar BranchServer.jar S3 "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt" &

java -jar BranchGUIDriver.jar "config/case2_script.txt" "config/case2_topology.txt" "config/case2_jvm.txt" "config/case2_branch.txt"
