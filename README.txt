1. Directory Structure

This zip package is a root directory of an Eclipse workspace.
It can be imported by selecting [File > Import... > Existing Projects into Workspace]
Windows batch file is provided at "/" to compile and create JAR files.

The directory organization is as follows:

/               - stores LOGIC.txt TEAM.txt README.txt TOPOexp.txt HighLevelDesign.txt
/src            - stores all source code files
/bin            - stores compiled class files
/manifest_xxx   - manifest files used to manually create each of the four JAR files
/res            - stores Ant buildfile and executable JAR files of BranchGUI, BranchServer,
                  FaultDetector and BranchGUIDriver, as well as test case launher (LAUNCH_caseN.cmd)
    /res/config - stores test case 1 to 4
    /res/log    - stores log of the latest execution in the form of
                  @[JVM-Name] ([BranchCode-Module]) : Message
                  a log item of ClientModule of branch 10 at JVM named G1 would look like the following 
                  @G1 (10-CM) : received UpdateHistResponse uid=1090032295 in ClientModule.sendUpdateHist():103 
      

2. How to Compile

2.1 Using Command Prompt (Windows)
Run compile.bat in /res
This will create four executable JARs: BranchGUI.jar, BranchServer.jar, 
BranchGUIDriver.jar and FailureDetector.jar

2.2 Using Eclipse
The developer uses Eclipse (Indigo) to code, and Java SE6 version 1.6.0_33 to compile.
The easiest way to compile is to import an Eclipse workspace and execute Ant build (build.xml).
To compile from command line/shell prompt, change the working directory to /res and execute "ant"
Ant will create four executable JARs: BranchGUI.jar, BranchServer.jar, 
BranchGUIDriver.jar and FailureDetector.jar


3. How to Run

Change working directory to /res then...
3.1 To run one of the four test cases,   execute LAUNCH_caseN.cmd
    launch script is in "/res", test script detail is in "/res/caseN_script.txt"
3.2 To run a standalone FaultDetector,   execute "java -jar FaultDetector.jar   [topology] [jvm] [branch]"
  - FaultDetector must be run before BranchGUI and BranchServer since they will report their dependency to the FaultDetector.
3.3 To run a standalone BranchGUI,       execute "java -jar BranchGUI.jar       [Branch Code]  [topology] [jvm] [branch]"
3.4 To run a standalone BranchServer,    execute "java -jar BranchServer.jar    [Branch Code]  [topology] [jvm] [branch]"
3.5 To run a standalone BranchGUIDriver, execute "java -jar BranchGUIDriver.jar [testcasefile] [topology] [jvm] [branch]"

NOTE: examples of [topology] [jvm] and [branch] setting file are provided in "/res"
      test case config files (caseN_xxxx.txt) also serve as good reference.
      
4. How to Test

Run the predefined script described in 3.1 or run separate components as describe in 3.2-3.5
