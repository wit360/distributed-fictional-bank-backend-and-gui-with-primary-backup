import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class BranchGUI extends JFrame implements ActionListener {
	
	Channel      mChannel; // channel wrapper (mainly send)
	Config       mConfig;  // configuration file
	ClientModule mClient;  // client of primary/backup protocol
	
	JTextField txtSrcAccountNumber = new JTextField(15);
	JTextField txtDstAccountNumber = new JTextField(15);
	JTextField txtAmount           = new JTextField(15);
	JTextField txtSerialNumber	   = new JTextField(15);
	JTextField txtBalance          = new JTextField(15);
	
	JButton btnDeposit  = new JButton("Deposit");
	JButton btnWithdraw = new JButton("Withdraw");
	JButton btnTransfer = new JButton("Transfer");
	JButton btnQuery    = new JButton("Query");

	JPanel panMain   = new JPanel();
	JPanel panCenter = new JPanel();
	JPanel panUserButton = new JPanel();
		
	// monitor message queue and process each.
	// use swingworker because GUI need to be updated.
	// as of now BranchGUI only process SHOW_BALANCE message
	private class ReceiveMessageTask extends SwingWorker<Void, Msg> {
		@Override
		protected Void doInBackground() {
			while(true){
				try {
					// bubble("ready for new message");
					Msg m = mChannel.receive(); // block here if there is no message
					publish(m);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}		
		}
		
		protected void process(java.util.List<Msg> mlist) {
			for( Msg m : mlist) {
				DriverMsg dm;
				FDMsg fm;
				BMsg bm;
				bubble("Message Loop Received: " + m);
				switch(m.type){
				case BMsg.SHOW_BALANCE:
					bm = new BMsg( (BMsg)m );
					mClient.receiveResponse(m);
					txtBalance.setText(String.valueOf(bm.amount) + " (A/C No." + bm.srcAccount + ")");
					txtAmount.requestFocus();
					txtAmount.selectAll();
					// message received! enable controls
					setUI(true);
					break;
				case BMsg.SEND_FAILED:
					showMessage("All servers failed");
					setUI(true);
					break;
				// ===========================================
				// P/B Protocol: failure(p)
				// ===========================================
				case FDMsg.SERVER_DOWN_NOTIFY:
					fm = new FDMsg( (FDMsg)m );
					bubble("FD said "+ fm.jvm_param + " went offline");
					mClient.serverDownNotify(fm.jvm_param);
					break;
					
				case FDMsg.SERVER_UP_NOTIFY:
					fm = new FDMsg( (FDMsg)m );
					bubble("FD said "+ fm.jvm_param + " went online " + mClient.servers.size());
					mClient.receiveRequest(fm);
					break;
				
				// from driver
				case DriverMsg.DRIVER_DEPOSIT:
					dm = (DriverMsg)m;
					txtSrcAccountNumber.setText(dm.srcAccount);
					txtAmount.setText(String.valueOf(dm.amount));
					txtSerialNumber.setText(dm.serialNumber);
					btnDeposit.doClick();
					break;
				case DriverMsg.DRIVER_WITHDRAW:
					dm = (DriverMsg)m;
					txtSrcAccountNumber.setText(dm.srcAccount);
					txtAmount.setText(String.valueOf(dm.amount));
					txtSerialNumber.setText(dm.serialNumber);
					btnWithdraw.doClick();
					break;
				case DriverMsg.DRIVER_TRANSFER:
					dm = (DriverMsg)m;
					txtSrcAccountNumber.setText(dm.srcAccount);
					txtDstAccountNumber.setText(dm.dstAccount);						
					txtAmount.setText(String.valueOf(dm.amount));
					txtSerialNumber.setText(dm.serialNumber);
					btnTransfer.doClick();
					break;
				case DriverMsg.DRIVER_QUERY:
					dm = (DriverMsg)m;
					txtSrcAccountNumber.setText(dm.srcAccount);
					txtSerialNumber.setText(dm.serialNumber);
					btnQuery.doClick();
					break;	
				}
			}
		}
	}
	private void setUI(boolean b){
		txtSrcAccountNumber.setEnabled(b);
		txtDstAccountNumber.setEnabled(b);
		txtAmount.setEnabled(b);
		txtSerialNumber.setEnabled(b);
		btnDeposit.setEnabled(b);
		btnWithdraw.setEnabled(b);
		btnTransfer.setEnabled(b);
		btnQuery.setEnabled(b);
	}
	
	// handle button events
	public void actionPerformed(ActionEvent arg0) {
		if( mClient.servers.isEmpty() ) {
			showMessage("All branch servers went offline :(");
			return;
		}
		BMsg message = new BMsg();

		// validate and set source account number
		if(!validate("Source A/C No.", txtSrcAccountNumber.getText(), new AccountNumberValidator())) return;
		message.srcAccount  = txtSrcAccountNumber.getText();
		
		// validate and set destination account number (only for transfer command)
		if(arg0.getSource() == btnTransfer) {
			if(!validate("Destination A/C No.", txtDstAccountNumber.getText(), new AccountNumberValidator())) return;
			message.dstAccount 	= txtDstAccountNumber.getText() ;
		}

		// validate and set amount (except query command)
		if(arg0.getSource() != btnQuery) {
			if(!validate("Amount", txtAmount.getText(), new IntegerValidator())) return;
			message.amount 		= Integer.parseInt(txtAmount.getText());
		}
		
		// validate and set serial number
		// fixed from assignment 1
		// message.serialNumber = this.generateSerialNumber();
		
		if(!validate("Serial Number", txtSerialNumber.getText(), new InputValidator())) return;
		message.serialNumber = txtSerialNumber.getText().trim();
		message.srcBC  = message.srcAccount.substring(0,2);
		message.dstBC  = message.srcAccount.substring(0,2);
		
		// set transaction type according to the clicked button
		if(arg0.getSource() == btnDeposit)       message.type = BMsg.DEPOSIT;			
		else if(arg0.getSource() == btnWithdraw) message.type = BMsg.WITHDRAW;
		else if(arg0.getSource() == btnTransfer) message.type = BMsg.TRANSFER;
		else if(arg0.getSource() == btnQuery)    message.type = BMsg.QUERY;
		else return; // impossible...

		// send BMsg disable controls
		setUI(false);
		mClient.receiveRequest(message);			
		// JOptionPane.showMessageDialog(this,message.toString());
	}
	
	// Initialize Channel and create user interface
	public BranchGUI(String jvm, String topofile, String jvmfile, String branchfile){
		super("CS 5414 Bank Branch [" + jvm +"]");
		mConfig = new Config(topofile, jvmfile, branchfile);
		mChannel = new Channel(jvm, mConfig );
		
		// initialize client module
		for(String branch: mConfig.mBranchGUI.keySet()) {
			System.out.println(branch + " " + jvm);
			if(mConfig.mBranchGUI.get(branch).equals(jvm)) {
				mClient = new ClientModule(mChannel,jvm,branch, mConfig.mBranchServers.get(branch));
				break;
			}
		}
		
		// create a new thread (SwingWorker) to monitor incoming message from a queue
		(new ReceiveMessageTask()).execute();
		
		// set event handlers
		btnDeposit.addActionListener(this);
		btnWithdraw.addActionListener(this);
		btnTransfer.addActionListener(this);
		btnQuery.addActionListener(this);
		
		// fill center panel with 4 textfields
		panCenter.setLayout( new GridBagLayout() );
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.LINE_START;
		
		// 1st line (source account number)
		gbc.gridy++;
		gbc.insets = new Insets(15,15,5,15);
		gbc.gridx=0;	panCenter.add(new JLabel("Source A/C No."),gbc);
		gbc.gridx=1;	panCenter.add(txtSrcAccountNumber,gbc);

		// 2nd line (destination account number)
		gbc.gridy++;
		gbc.insets = new Insets(0,15,5,15);
		gbc.gridx=0;	panCenter.add(new JLabel("Destination A/C No."),gbc);
		gbc.gridx=1;	panCenter.add(txtDstAccountNumber,gbc);
		
		// 3rd line (amount)
		gbc.gridy++;
		gbc.insets = new Insets(0,15,5,15);
		gbc.gridx=0;	panCenter.add(new JLabel("Amount"),gbc);
		gbc.gridx=1;	panCenter.add(txtAmount,gbc);
		
		// 3rd line (amount)
		gbc.gridy++;
		gbc.insets = new Insets(0,15,5,15);
		gbc.gridx=0;	panCenter.add(new JLabel("Serial No."),gbc);
		gbc.gridx=1;	panCenter.add(txtSerialNumber,gbc);

		// 5th line (balance)
		gbc.gridy++;
		gbc.gridx=0;	panCenter.add(new JLabel("Balance"),gbc);
		gbc.gridx=1;	panCenter.add(txtBalance,gbc);
		txtBalance.setEditable(false);
		
		// 6th line (Bank Button)
		gbc.gridy++;
		gbc.insets = new Insets(0,5,0,5);
		gbc.gridx=0;
		gbc.gridwidth=2;	panCenter.add(panUserButton,gbc);		
		
		// fill bottom panel with buttons
		panUserButton.add(btnDeposit);
		panUserButton.add(btnWithdraw);
		panUserButton.add(btnTransfer);
		panUserButton.add(btnQuery);
		
		// place panels
		panMain.setLayout(new BorderLayout());
		panMain.add(panCenter,BorderLayout.CENTER);
		this.getContentPane().add(panMain);
		
		// show main window
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// User Input Validators
	interface BankGUIValidator{
		public boolean validate(String userInput);
	}
	
	// for account number
	class AccountNumberValidator implements BankGUIValidator{
		public boolean validate(String accountNumber){
			if(accountNumber.length()!=8) return false;
			try{
				if(accountNumber.charAt(2) != '.') return false;
				Integer.parseInt(accountNumber.substring(0,2));
				Integer.parseInt(accountNumber.substring(3,8));
			} catch (Exception e) {
				return false;
			}
			return true;
		}
	}
	
	// anything integer
	class IntegerValidator implements BankGUIValidator{
		public boolean validate(String amount){
			try{ 
				int i = Integer.parseInt(amount);
				return i>0; // only allow positive number
			} catch(Exception e){ 
				return false; 
			}
		}
	}
	
	// empty test
	class InputValidator implements BankGUIValidator{
		public boolean validate(String input){
			return !input.trim().equals("");
		}
	}
	
	private boolean validate(String fieldName, String userInput, BankGUIValidator validator){
		if( validator.validate(userInput) ) return true;
		JOptionPane.showMessageDialog(this,"Please enter '" + fieldName + "' correctly");
		return false;
	}

	private void showMessage(String msg){
		JOptionPane.showMessageDialog(this,msg);
	}

	private void bubble(String s){
        System.out.println(String.format("@%-11s: ",mChannel.getMyJVM() + " (GUI)") + s);
    }
	
	// BranchGUI application entry point
	public static void main(final String[] args) {	
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){ new BranchGUI(
					args.length > 0 ? args[0] : "G1",
					args.length > 1 ? args[1] : "topology_file.txt",
					args.length > 2 ? args[2] : "jvm_file.txt",
					args.length > 3 ? args[3] : "branch_file.txt"
				);
			}
		});
	}
}
