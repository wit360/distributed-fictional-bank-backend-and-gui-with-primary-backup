import java.io.BufferedReader;
import java.io.FileReader;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

public class Config{
	// default configuration files
	String TOPOLOGY_FILE = "topology_file.txt";
	String JVM_FILE      = "jvm_file.txt";
	String BRANCH_FILE   = "branch_file.txt";

	public HashMap<String,HashSet<String>>    mNeighborJVMs     = new HashMap<String,HashSet<String>>();    // jvm list given jvm
	public HashMap<String,HashSet<String>>    mNeighborBranches = new HashMap<String,HashSet<String>>();    // branch list given branch
	public HashMap<String,HashSet<String>>    mJVMBranches      = new HashMap<String,HashSet<String>>();    // branch list given jvm
	public HashMap<String,InetSocketAddress>  mJVMAddress       = new HashMap<String,InetSocketAddress>();  // ip address info
	public HashMap<String,TreeSet<JVMServer>> mBranchServers    = new HashMap<String,TreeSet<JVMServer>>(); // branch p/b info
	public HashMap<String,String>			  mBranchGUI        = new HashMap<String,String>();             // client info
	public String 							  mFaultDetectorJVM = null;
	
	public Config(){
		readTopology();
		readJVM();
		readBranch();
	}
	
	public Config(String topo, String jvm, String branch){
		TOPOLOGY_FILE = topo;
		JVM_FILE      = jvm;
		BRANCH_FILE   = branch;
		readTopology();
		readJVM();
		readBranch();		
	}
	
	private void readTopology(){
		try {
			String line; // temp line buffer
			BufferedReader tin = new BufferedReader(new FileReader(TOPOLOGY_FILE));			
			while( (line = tin.readLine()) != null ) {
				line = line.trim();
				if(line.startsWith("exit")) return;
				if(line.length() < 3 || line.startsWith("#")) continue;
				String[] tokens = line.trim().split("\\s+");
				if( tokens.length < 2 ) continue;
				// sample TOPOLOGY_FILE
				// JVM1 JVM2
				String srcJVM = tokens[0];
				String dstJVM = tokens[1];
				if( !mNeighborJVMs.containsKey(srcJVM) ) {
					HashSet<String> neighbors = new HashSet<String>();
					neighbors.add(srcJVM);
					mNeighborJVMs.put(srcJVM, neighbors);
				}
				mNeighborJVMs.get(srcJVM).add(dstJVM);
			}
			tin.close();
		} catch (Exception e){ e.printStackTrace(); }
	}
	
	private void readJVM(){
		try{
			String line; // temp line buffer
			BufferedReader tin = new BufferedReader(new FileReader(JVM_FILE));			
			while( (line = tin.readLine()) != null ) {
				line = line.trim();
				if(line.startsWith("exit")) return;
				if(line.length() < 3 || line.startsWith("#")) continue;
				String[] tokens = line.trim().split("\\s+");
				if( tokens.length < 4 ) continue;
				// sample JVM_FILE
				// SERVER 	   10_1     127.0.0.1   40001
				// GUI         10_0     127.0.0.1   60001
				boolean isServer        = tokens[0].toLowerCase().startsWith("s");
				boolean isFaultDetector = tokens[0].toLowerCase().startsWith("f");
				boolean isGui           = tokens[0].toLowerCase().startsWith("g");
				if( !(isServer || isFaultDetector || isGui) ) continue;
				String jvm    = tokens[1];
				String ip     = tokens[2];
			    int    port   = Integer.parseInt(tokens[3]);
			    mJVMAddress.put(jvm ,new InetSocketAddress(ip,port));
			    if( isFaultDetector ) this.mFaultDetectorJVM = jvm;
			}
			tin.close();
		} catch (Exception e){ e.printStackTrace(); }
	}
	
	private void readBranch(){
		try{
			String line; // temp line buffer
			BufferedReader tin = new BufferedReader(new FileReader(BRANCH_FILE));			
			while( (line = tin.readLine()) != null ) {
				line = line.trim();
				if(line.startsWith("exit")) return;
				if(line.length() < 3 || line.startsWith("#")) continue;
				String[] tokens = line.trim().split("\\s+");
				if( tokens.length < 3 ) continue;
				// sample BRANCH_FILE (branchcode gui primary backups ...
				// 10             G1    S1 S2 S4
				String branch  = tokens[0];
				String gui     = tokens[1];
				mBranchGUI.put(branch, gui);
				TreeSet<JVMServer> servers = new TreeSet<JVMServer>();
				String primary = tokens[2];
				
				// jvm to branch mapping
				this.addNew(mJVMBranches,primary, branch);
				this.addNew(mJVMBranches, gui, branch);
				int pid = 1;
				servers.add(new JVMServer(primary,pid++));
				for(int i=3; i<tokens.length; i++) {
					String jvm = tokens[i];
					this.addNew(mJVMBranches,jvm, branch);
					servers.add(new JVMServer(jvm,pid++));
				}
				mBranchServers.put(branch, servers);
				
				// generate neighbor branches
				for(String a_jvm: mNeighborJVMs.keySet()) {
					if(!mNeighborJVMs.containsKey(a_jvm)) continue;
					for(String neighbor_jvm: mNeighborJVMs.get(a_jvm)) {
						if(!mJVMBranches.containsKey(a_jvm)) continue;
						for(String a_branch: mJVMBranches.get(a_jvm)){
							if(!mJVMBranches.containsKey(neighbor_jvm)) continue;
							for(String neighbor_branch: mJVMBranches.get(neighbor_jvm)){
								this.addNew(mNeighborBranches, a_branch, neighbor_branch);
							}
						}
					}
				}
			}
			tin.close();
		} catch (Exception e){ e.printStackTrace(); }
	}
	
	private void addNew(HashMap<String,HashSet<String>> map, String key, String value){
		if( !map.containsKey(key) ) map.put(key, new HashSet<String>());
		map.get(key).add(value);
	}
	
	public static void main(String args[]){
		Config c = new Config();
		c.dumpTopology();
		c.dumpAddress();
		c.dumpBranch();
	}
	
	public void dumpTopology(){
		System.out.println("topology");
		for(String key: mNeighborJVMs.keySet()){
			System.out.println(key + " " + mNeighborJVMs.get(key) + " branches=" + mJVMBranches.get(key));
		}
	}
	public void dumpAddress(){
		System.out.println("address");
		for(String key: mJVMAddress.keySet()){
			InetSocketAddress isa =  mJVMAddress.get(key);
			System.out.println(key + " " + isa.getAddress().getHostAddress() + ":" + isa.getPort());
		}
	}
	public void dumpBranch(){
		System.out.println("branches");
		for(String key: mBranchServers.keySet()){
			System.out.println(key + " gui=" + mBranchGUI.get(key) + " servers=" + mBranchServers.get(key)
					+ " neighbors=" + mNeighborBranches.get(key));
		}
	}
}