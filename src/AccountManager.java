import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

// account information for each account number (only balance as of now)
@SuppressWarnings("serial")
class AccountInfo implements Serializable{	
	public int balance;
	public HashSet<String> serials = new HashSet<String>(); 
}

@SuppressWarnings("serial")
class AccountManager implements Serializable{
	public HashMap<String,AccountInfo> accounts = new HashMap<String,AccountInfo>();
	private String mMyBranchCode;
	
	public AccountManager(String myBranchCode){
		mMyBranchCode = myBranchCode;
	}
	
	public void deposit(String accountNumber, String serialNumber, int amount) throws DuplicateSerialNumberException, NotMyAccountException{
		checkSerialNumber(accountNumber,serialNumber);
		checkAccount(accountNumber);
		accounts.get(accountNumber).balance += amount;
	}
	
	public void withdraw(String accountNumber, String serialNumber, int amount) throws DuplicateSerialNumberException, NotMyAccountException{
		checkSerialNumber(accountNumber,serialNumber);
		checkAccount(accountNumber);
		accounts.get(accountNumber).balance -= amount;
	}
	
	public int query(String accountNumber) throws NotMyAccountException {
		// no need to check serial number
		checkAccount(accountNumber);
		return accounts.get(accountNumber).balance;
	}
	
	private void checkAccount(String accountNumber) throws NotMyAccountException {
		// check if the account is managed by my branch
		String branchCode = accountNumber.substring(0,2);
		if(!branchCode.equals(mMyBranchCode)) throw new NotMyAccountException();
		
		if(accounts.containsKey(accountNumber)) return;
		
		// create a new account if necessary
		AccountInfo newAccount = new AccountInfo();
		newAccount.balance = 0;
		accounts.put(accountNumber,newAccount);		
	}
	
	private void checkSerialNumber(String accountNumber, String serialNumber) throws DuplicateSerialNumberException, NotMyAccountException{
		// check if the account is managed by my branch
		String branchCode = accountNumber.substring(0,2);
		if(!branchCode.equals(mMyBranchCode)) throw new NotMyAccountException();
		
		// check serial number
		if( accounts.containsKey(accountNumber)){
			HashSet<String >serials = accounts.get(accountNumber).serials;

			if( !serials.contains(serialNumber)) {
				serials.add(serialNumber);
			} else {
				// throw exception for invalid serial number
				throw new DuplicateSerialNumberException();
			}
		} else {
			AccountInfo newAccount = new AccountInfo();
			newAccount.balance = 0;
			newAccount.serials.add(serialNumber);
			accounts.put(accountNumber,newAccount);	
		}
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		for(String accno: accounts.keySet()){
			sb.append(String.format(" %s's balance = %d\n",accno,accounts.get(accno).balance));
		}
		
		return sb.toString();
	}
}

@SuppressWarnings("serial")
class DuplicateSerialNumberException extends Exception{}
@SuppressWarnings("serial")
class NotMyAccountException extends Exception{}