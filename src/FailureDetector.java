import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class FailureDetector extends JFrame implements ActionListener{
	Config  mConfig;
	Channel mChannel;
	
	HashMap<String,HashSet<String>> dependency = new HashMap<String,HashSet<String>>(); // jvm and a set of dependents
	HashMap<String,Boolean> serverStatus = new HashMap<String,Boolean>(); // each jvm's running status (true=normal, false=failed)
	ArrayList<String> components = new ArrayList<String>(); // sorted list of jvm names
	DependencyModel listmodel = new DependencyModel();      // list model

	JPanel panCenter  = new JPanel();
	JPanel panTop     = new JPanel();
	JPanel panBottom  = new JPanel();
	JList  serverList = new JList(listmodel);
	JButton cmdDown   = new JButton("Down");
	JButton cmdDownN  = new JButton("Down Notify");
	JButton cmdUp     = new JButton("Up");
	JButton cmdUpN    = new JButton("Up Notify");
	
	// model that populate JList(serverList) data by reading 
	// the above "components" and "dependency" data structure
	class DependencyModel extends AbstractListModel{
		public void fireUpdated(){
			this.fireContentsChanged(this, 0,getSize()-1);
		}
		public void fireAdded(){
			this.fireIntervalAdded(this, getSize()-1, getSize()-1);
		}
		
		@Override
		public Object getElementAt(int index) {
			String parent = components.get(index);
			StringBuffer sb = new StringBuffer();
			sb.append(" " + parent);
			sb.append( serverStatus.get(parent) ? " up\t<- ":" down\t<- " );
			ArrayList<String> list = new ArrayList<String>(dependency.get(parent));
			Collections.sort(list);

			int i=0;
			for(String dependent : list){
				if(i++ > 0)	sb.append(", " + dependent);
				else sb.append("[" + dependent);
			}
			sb.append("]");
			return sb.toString();
		}
		@Override
		public int getSize() {
			return components.size();
		}
	}
	
	// monitor message queue and process each.
	// use swingworker because GUI need to be updated.
	private class ReceiveMessageTask extends SwingWorker<Void, Msg> {
		@Override
		protected Void doInBackground() throws Exception{
			while(true){
				try {
					Msg m = mChannel.receive(); // block here if there is no message
					publish(m);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}		
		} 
		protected void process(java.util.List<Msg> msg) {
			for( Msg m : msg) {
				DriverMsg dm;
				FDMsg fm;
				switch(m.type){
				// registration from real BranchServer and BranchGUI
				case FDMsg.REGISTER:
					fm = (FDMsg)m;
					for( String parent : fm.jvm_dependency){
						if(!dependency.containsKey(parent)) {
							HashSet<String> dependents = new HashSet<String>();
							dependents.add( fm.srcJVM );
							components.add(parent);
							serverStatus.put( parent, true );
							dependency.put(parent, dependents);
							listmodel.fireAdded();
						} else {
							dependency.get(parent).add( fm.srcJVM );
						}
					}
					break;
					
				// from driver
				case DriverMsg.DRIVER_DOWN:
					dm = (DriverMsg)m;
					serverDown(dm.jvm_param);
					break;
				// from driver
				case DriverMsg.DRIVER_UP:
					dm = (DriverMsg)m;
					serverUp(dm.jvm_param);
					break;
				// from driver
				case DriverMsg.DRIVER_DOWN_NOTIFY:
					dm = (DriverMsg)m;
					serverDownNotify(dm.jvm_param);
					break;
				// from driver
				case DriverMsg.DRIVER_UP_NOTIFY:
					dm = (DriverMsg)m;
					serverUpNotify(dm.jvm_param);
					break;
				}
				Collections.sort(components);
				listmodel.fireUpdated();
			}
		}
	}
	
	public FailureDetector(String topo, String jvm, String branch){
		super("Fault Detector");
		
		mConfig = new Config(topo,jvm,branch);
		mChannel = new Channel(mConfig.mFaultDetectorJVM, mConfig);
		
		// create a new thread (SwingWorker) to monitor incoming message from a queue
		(new ReceiveMessageTask()).execute();
		serverList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		serverList.setLayoutOrientation(JList.VERTICAL);

		JScrollPane listScroller = new JScrollPane(serverList);
		listScroller.setPreferredSize(new Dimension(800, 800));
		
		panCenter.setLayout(new BorderLayout());
		panBottom.setLayout(new FlowLayout());
		panBottom.add(cmdUp);
		panBottom.add(cmdDown);
		panBottom.add(cmdUpN);
		panBottom.add(cmdDownN);
		panTop.add( new JLabel("Components of Interest (with dependents)") );
		panCenter.add(panTop, BorderLayout.NORTH);
		panCenter.add(listScroller,BorderLayout.CENTER);
		panCenter.add(panBottom, BorderLayout.SOUTH);
		
		this.getContentPane().add(panCenter);
		
		cmdUp.addActionListener(this);
		cmdDown.addActionListener(this);
		cmdUpN.addActionListener(this);
		cmdDownN.addActionListener(this);
		
		// show main window
		this.setSize(450,200);
		this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		for(int index : serverList.getSelectedIndices()){
			String server = components.get(index);
			if(ae.getSource()==cmdUp){
				this.serverUp(server);
			} else if(ae.getSource()==cmdDown){
				this.serverDown(server);
			} else if(ae.getSource()==cmdUpN){
				this.serverUpNotify(server);
			} else if(ae.getSource()==cmdDownN){
				this.serverDownNotify(server);
			}
		}
	}
	
	private void serverUp(String server){
		FDMsg faultinfo = new FDMsg();
		faultinfo.type = FDMsg.SERVER_UP;
		faultinfo.dstJVM = server;
		faultinfo.jvm_param = server;
		try {
			// first send SERVER_UP to recover the server
			mChannel.sendToJVM(faultinfo);	
		} catch (Exception e) {
			System.out.println("unable to send messages to " + server);
			// e.printStackTrace();
		}
	}
	private void serverDown(String server){
		FDMsg faultinfo = new FDMsg();
		faultinfo.type = FDMsg.SERVER_DOWN;
		faultinfo.dstJVM = server;
		faultinfo.jvm_param = server;
		try {
			mChannel.sendToJVM(faultinfo);
		} catch (Exception e) {
			System.out.println("unable to send SERVER_DOWN to " + server);
			// e.printStackTrace();
		}
	}	
	private void serverUpNotify(String parent){
		boolean isOnline = serverStatus.get(parent);
		if(isOnline) return; // already online
		serverStatus.put(parent, true);
		listmodel.fireUpdated();

		FDMsg faultinfo = new FDMsg();
		faultinfo.type = FDMsg.SERVER_UP_NOTIFY;
		faultinfo.jvm_param = parent;
		
		for(String dependent : dependency.get(parent)) {
			try {
				faultinfo.dstJVM = dependent;
				mChannel.sendToJVM(faultinfo);
			} catch (Exception e) {
				System.out.println("unable to send SERVER_UP_NOTIFY to " + dependent);
				// e.printStackTrace();
			}
		}
		
	}
	
	private void serverDownNotify(String parent){
		boolean isOnline = serverStatus.get(parent);
		if(!isOnline) return; // already offline
		serverStatus.put(parent, false);
		listmodel.fireUpdated();
		
		FDMsg faultinfo = new FDMsg();
		faultinfo.type = FDMsg.SERVER_DOWN_NOTIFY;

		faultinfo.jvm_param = parent;
		for(String dependent : dependency.get(parent)) {
			try {
				faultinfo.dstJVM = dependent;				
				mChannel.sendToJVM(faultinfo);
			} catch (Exception e) {
				System.out.println("unable to send SERVER_DOWN_NOTIFY to " + dependent);
				// e.printStackTrace();
			}
		}	
	}
	
	public static void main(final String[] args){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){ 
				new FailureDetector(
					args.length > 0 ? args[0] : "topology_file.txt",
					args.length > 1 ? args[1] : "jvm_file.txt",
					args.length > 2 ? args[2] : "branch_file.txt"
				);
			}
		});
	}
}