import java.io.BufferedReader;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class BranchGUIDriver {

	// map neighbor jvm to ip address/port 
	// map each JVM name to ip address/port
	Config config;
	// HashMap<String,InetSocketAddress>  mBranchCode2SocketAddress = new HashMap<String,InetSocketAddress>();
	public static final int DEFAULT_DELAY_MS = 500;
	
	// execute a line from testscript
	public void executeScript(String[] tokens) throws Exception{
//	# deposit 		[FRONT_JVM]  deposit  [Serial No] [Account No] [Amount]
//	# withdraw 		[FRONT_JVM]  withdraw [Serial No] [Account No] [Amount]
//	# query 		[FRONT_JVM]  query    [Serial No] [Account No]
//	# transfer 		[FRONT_JVM]  transfer [Serial No] [Source Account No] [Destination Account No] [Amount]
//	# snapshot		[FRONT_JVM]  snapshot 
//	# server down	[FD_JVM]     s-down   [FAILED_JVM]	
//	# server up 	[FD_JVM]     s-up     [FAILED_JVM]
//	# notify down	[FD_JVM]     n-down   [FAILED_JVM]	
//	# notify up 	[FD_JVM]     n-up     [FAILED_JVM]
//	# delay     	delay  [Interval in millisecond]
		if(tokens[0].toLowerCase().equals("delay")){
			if(tokens.length==2) delay(Integer.parseInt(tokens[1]));
			else delay();
		} else if (tokens[0].toLowerCase().startsWith("exit")) { 
			System.exit(0);
		} else if( config.mJVMAddress.containsKey(tokens[0])){
			if(tokens.length < 2) { System.out.println("Not enough argument"); return;}
			// command issue to a jvm
			DriverMsg bm = new DriverMsg();
			if( tokens[1].toLowerCase().startsWith("d")) { 
				if(tokens.length < 5) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_DEPOSIT;
				bm.serialNumber = tokens[2];
				bm.srcAccount = tokens[3];
				bm.amount = Integer.parseInt(tokens[4]);
			} else if( tokens[1].toLowerCase().startsWith("w")) {
				if(tokens.length < 5) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_WITHDRAW;
				bm.serialNumber = tokens[2];
				bm.srcAccount = tokens[3];
				bm.amount = Integer.parseInt(tokens[4]);
			} else if( tokens[1].toLowerCase().startsWith("t")) {
				if(tokens.length < 6) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_TRANSFER;
				bm.serialNumber = tokens[2];
				bm.srcAccount = tokens[3];
				bm.dstAccount = tokens[4];
				bm.amount = Integer.parseInt(tokens[5]);				
			} else if( tokens[1].toLowerCase().startsWith("q")) {
				if(tokens.length < 4) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_QUERY;
				bm.serialNumber = tokens[2];
				bm.srcAccount = tokens[3];				
			} else if( tokens[1].toLowerCase().startsWith("s-d")) {
				if(tokens.length < 3) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_DOWN;
				bm.jvm_param = tokens[2];
			} else if( tokens[1].toLowerCase().startsWith("n-d")) {
				if(tokens.length < 3) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_DOWN_NOTIFY;
				bm.jvm_param = tokens[2];
			} else if( tokens[1].toLowerCase().startsWith("s-u")) {
				if(tokens.length < 3) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_UP;
				bm.jvm_param = tokens[2];
			} else if( tokens[1].toLowerCase().startsWith("n-u")) {
				if(tokens.length < 3) { System.out.println("Not enough argument"); return;}
				bm.type = DriverMsg.DRIVER_UP_NOTIFY;
				bm.jvm_param = tokens[2];
			}
			this.sendDriverMsgToJVM(bm, tokens[0]); // send!
		}
	}
	
	// send a DriverMsg to a particular JVM name
	public void sendDriverMsgToJVM(DriverMsg bm,String jvm) throws Exception{
		// connect and send message and close
		try{
		InetSocketAddress isa = config.mJVMAddress.get(jvm);
		Socket sendSocket = new Socket(isa.getAddress(), isa.getPort());
		ObjectOutputStream oos = new ObjectOutputStream(sendSocket.getOutputStream());
		oos.writeObject(bm);
		sendSocket.close();
		} catch(Exception e){ System.out.println("- error sending a message.");}
	}

	public void delay(){
		delay(BranchGUIDriver.DEFAULT_DELAY_MS);
	}
	public void delay(int ms){
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public static void main(String[] args) throws Exception {
		if(args.length<4) {
			System.out.println("Please specify [testscript] [topology] [jvm] file");
			System.exit(0);
		}
		
		BranchGUIDriver driver  = new BranchGUIDriver(args[1],args[2],args[3]);
		
		// read a file from argument
		BufferedReader bin = new BufferedReader(new FileReader(args[0]));
		String line;
		int lineNo = 0;
		while( (line = bin.readLine()) != null ) {
			lineNo++;
			String tline = line.trim();
			if(tline.length() < 3 || tline.startsWith("#")) continue;
			System.out.println(String.format("[ TESTSCRIPT line %03d ] > %s", lineNo,line));
			String[] tokens = tline.split("\\s+");
			driver.executeScript(tokens);
		}
		bin.close();
	}
	
	public BranchGUIDriver(String topo, String jvm, String branch){
		config = new Config(topo,jvm,branch);		
	}
}
