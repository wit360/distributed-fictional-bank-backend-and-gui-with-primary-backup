import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

class Channel {
	HashMap<String,Socket>     mNeighborJVM2Socket  = new HashMap<String,Socket>();    // manage outging sockets
	LinkedBlockingQueue<Msg>   mReceiveQueue        = new LinkedBlockingQueue<Msg>();  // received BankMessage queue
	Config                     mConfig;
	protected String 	       mMyJVM;
	
	public Channel(String jvm, Config config){
		mConfig = config;
		mMyJVM  = jvm;
		(new TCPServerThread(this)).start(); // listen for incoming TCP connection
		
		if( !mMyJVM.equals(mConfig.mFaultDetectorJVM) ){
			FDMsg faultinfo = new FDMsg();
			faultinfo.type = FDMsg.REGISTER;
			faultinfo.dstJVM = mConfig.mFaultDetectorJVM;
			faultinfo.jvm_dependency = this.whoDependency();
			try {
				this.sendToJVM(faultinfo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void enqueueBankMessage(Msg m){ mReceiveQueue.add(m); }   // called by many TCPReceiverThread
	public Msg receive() throws InterruptedException {	return mReceiveQueue.take(); }
	public void handleNewConnection(Socket socket){ (new TCPReceiverThread(this,socket)).start();} // 
	
	public synchronized void sendToJVM(Msg m) throws IOException, NeighborNotFoundException{
		String dstJVM = m.dstJVM;
		if(dstJVM == null || !mConfig.mNeighborJVMs.containsKey(dstJVM)) throw new NeighborNotFoundException();
		// then get neighbor's IP/Port from JVMName
		m.srcJVM = mMyJVM;
		InetSocketAddress isa = mConfig.mJVMAddress.get(dstJVM);
		
		// send message (connect upon first try)
		if(!mNeighborJVM2Socket.containsKey(dstJVM)){
			mNeighborJVM2Socket.put(dstJVM, new Socket(isa.getAddress(), isa.getPort()));
		}
		Socket sendSocket = mNeighborJVM2Socket.get(dstJVM);
		try {
			ObjectOutputStream oos = new ObjectOutputStream(sendSocket.getOutputStream());
			oos.writeObject(m);

		} catch (SocketException se){
			sendSocket = new Socket(isa.getAddress(), isa.getPort());
			mNeighborJVM2Socket.put(dstJVM, sendSocket);
			ObjectOutputStream oos = new ObjectOutputStream(sendSocket.getOutputStream());
			oos.writeObject(m);
		}
	}
	
	// getters
	public String[] whoNeighbors(){	return mConfig.mNeighborJVMs.get(mMyJVM).toArray(new String[0]);}
	public String[] whoDependency(){
		HashSet<String>dependency = new HashSet<String>();
		for(String branch: mConfig.mJVMBranches.get(mMyJVM)){
			// System.out.println(mMyJVM + mConfig.mBranchServers.get(branch));
			for(JVMServer server: mConfig.mBranchServers.get(branch)){
				dependency.add(server.name);
			}
		}
		return dependency.toArray(new String[0]);
	}
	//public String getNeighborJVMFromBranch(String bc) { return mNeighborBranch2JVM.get(bc);}
	//public boolean isBranchReachable(String bCode) { return mNeighborBranch2JVM.containsKey(bCode);}
	
	public String getMyIPAddress(){ return mConfig.mJVMAddress.get(mMyJVM).getAddress().getHostAddress(); }
	public int    getMyPort() 	  {	return mConfig.mJVMAddress.get(mMyJVM).getPort(); }
	public String getMyJVM()      { return mMyJVM; }
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s -> connected to [branch_code]: primary(pid)->backup1(pid)->...->backup_n(pid)\n",getMyJVM()));
		for( String bc : mConfig.mNeighborBranches.get(mMyJVM) ){
			TreeSet<JVMServer> servers = mConfig.mBranchServers.get(bc);
			JVMServer server = servers.first();
			sb.append(String.format(" %5s : %s(%d)",bc,server.name,server.id));
			while( (server = servers.higher(server)) != null){
				sb.append(String.format(" -> %s(%d) ",server.name,server.id));	
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}

@SuppressWarnings("serial")
class NeighborNotFoundException extends Exception {}

//listen to the port specified in topology_file.txt
class TCPServerThread extends Thread{
	private static final int BACKLOG = 5;
	ServerSocket mServerSocket;
	Channel mChannel;
	
	// initialize ServerSocket by providing info from Channel
	public TCPServerThread(Channel Channel) {
		mChannel = Channel;
		try {
			mServerSocket = new ServerSocket(mChannel.getMyPort(), BACKLOG, 
					InetAddress.getByName(mChannel.getMyIPAddress()));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(mChannel.getMyJVM() + " IP " + mChannel.getMyIPAddress() + ":" + mChannel.getMyPort());
			e.printStackTrace();
		}
	}
	
	// listen to a new connnection forever
	public void run() {
		while(true){
			try {
				// System.out.println("Waiting for incoming TCP connection...");
				mChannel.handleNewConnection(mServerSocket.accept());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

// receive an object from tcp socket
class TCPReceiverThread extends Thread{
	Channel mChannel;
	Socket  mSocket;
	
	public TCPReceiverThread(Channel Channel, Socket socket){
		mChannel = Channel;
		mSocket = socket;
	}
	public void run() {
		try {
			// one object at a time
			while(true){
				ObjectInputStream ois = new ObjectInputStream(mSocket.getInputStream());
				Msg m = (Msg) ois.readObject();
				mChannel.enqueueBankMessage(m);
			}
		} catch (IOException ioe) {
			// ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			// cnfe.printStackTrace();
		}
	}
}


