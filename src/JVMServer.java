import java.io.Serializable;

// this class represent a jvm and its associated proceess id
@SuppressWarnings("serial")
class JVMServer implements Comparable<JVMServer>, Serializable{
	String 	name;
	int 	id;
	public JVMServer(String s, int n){
		name = s; // jvm name
		id   = n; // process id
	}
	@Override
	public int compareTo(JVMServer s2) {
		return this.id - s2.id;
	}
	public String toString(){
		return name + "(" + id + ")"; 
	}
}
