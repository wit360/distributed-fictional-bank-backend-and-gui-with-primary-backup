import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingQueue;

class ClientModule extends Thread{
	LinkedBlockingQueue<Msg>   requests = new LinkedBlockingQueue<Msg>();  // received BankMessage queue

	// Primary/Backup protocol variables
	ConcurrentHashMap<String,HashSet<Msg>>  responses    = new ConcurrentHashMap<String,HashSet<Msg>>();  // collect responses from replicas
	private final Object                    responseLock = new Object();
	public ConcurrentSkipListSet<JVMServer> servers;  // server replicas
	int     runningNumber = 0;
	int     responseCount = 0;

	String  mMyJVM 	  = null;
	String  mMyBranch = null;
	Channel mChannel  = null;
	boolean isDown    = false;
	
	public void receiveRequest(Msg m){requests.offer(m);}  // make request FIFO
	public void receiveResponse(Msg m){                    // manage responses set (hashed by source jvm)
		bubble("check 0 " + m.type + " " + m.uid);
		if(!responses.containsKey(m.srcJVM)) responses.put(m.srcJVM, new HashSet<Msg>());
		synchronized(responseLock){
			this.responseCount++;
			responses.get(m.srcJVM).add(m);
			responseLock.notifyAll();  // wake up threads that are waiting for response to check new response
		}
	}

	// for debug
	FileWriter logWriter;
	
	public ClientModule(Channel channel, String jvm, String branch, TreeSet<JVMServer> servers){
		try {logWriter = new FileWriter("log/" + jvm + "(" + branch + "-CM).txt");
		} catch (IOException e) {e.printStackTrace();}

		mMyJVM       = jvm;
		mMyBranch    = branch;
		mChannel     = channel;
		this.servers = new ConcurrentSkipListSet<JVMServer>(servers);
		this.start();
	}

	public void run(){
		while(true){
			Msg   m  = null;
			try { 
				// bubble("ready for new message");
				bubble("Waiting for new message");
				m = requests.take();
				if(isDown) {
					bubble("Down: ignoring " + m);      // ignore incoming message to simulate failure
					continue;
				}
				switch(m.type){
				case BMsg.DEPOSIT:
				case BMsg.WITHDRAW:
				case BMsg.TRANSFER:
				case BMsg.IB_TRANSFER:
				case BMsg.QUERY:
					try {
						this.sendUpdateHist( new BMsg((BMsg)m) );  // send request to BranchServer
					} catch (NeighborNotFoundException e) {
						e.printStackTrace();
					}
					break;
				case FDMsg.SERVER_UP_NOTIFY:                 // fault recovery
					FDMsg fm = new FDMsg( (FDMsg)m );
					this.serverUpNotify(fm.jvm_param);
				}
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			} 
		}
	}
	private void sendUpdateHist(BMsg bm) throws NeighborNotFoundException{
		bubble("check 1 " + bm.type + " " + bm.uid);
		while(!servers.isEmpty()){
			String primary = servers.first().name;
			int sendUid = bm.hashCode()+runningNumber++; // genUID
			bm.uid = sendUid;
			bm.srcJVM = mMyJVM;
			bm.dstJVM = primary;
			int lastResponseCount = this.responseCount;
			try {
				bubble("sending UpdateHist to " + bm.dstJVM);
				mChannel.sendToJVM(bm);
			} catch (IOException e1) {
				bubble("could not send to " + bm.dstJVM);
			} // SEND("updateHist", (uid,op)) to primary
			while(true){ // wait until 
				try {
					bubble("waiting for reply from primary | uid=" + bm.uid);
					synchronized(responseLock){
						if(lastResponseCount == this.responseCount) {
							responseLock.wait();
						}
						lastResponseCount = this.responseCount;
						if(isDown) return;
						// bubble("notified! primary=" + primary + " head=" + servers.first() + " " + servers.first().name.equals(primary));
						if(responses.containsKey(primary)) {
							for(Msg m: responses.get(primary)){
								if(m.uid == sendUid){  // (?,(uid,?)) in responses
									// should we clean up responses??
									bubble("received UpdateHistResponse uid=" + m.uid );
									return;
								}
							}
						} 
					}
				} catch (InterruptedException e) {}
				// all servers failed, get out of 2 while loops to send error response
				if( servers.isEmpty() ) {
					break; 
				} 
				// primary failed, get out of this loop to retransmit
				if(!servers.first().name.equals(primary) ) {
					bubble("primary failed");
					break;
				}
			}
		}
		bubble("all failed");
		BMsg sentFailed = new BMsg();
		sentFailed.type  = BMsg.SEND_FAILED;
		sentFailed.srcBC = mMyBranch;
		sentFailed.srcBC = mMyBranch;
		sentFailed.srcJVM = mMyJVM;
		sentFailed.dstJVM = bm.srcJVM;
		try {
			mChannel.sendToJVM(sentFailed);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void serverDownNotify(String jvm){
		if(isDown) return;
		if(responses.containsKey(jvm)) responses.put(jvm, new HashSet<Msg>());
		
		// remove failed jvm from the server list
		for(JVMServer server : servers){
			if(server.name.equals(jvm)) {
				bubble("removing " + jvm + " primary=" + servers.first().name);
				servers.remove(server);
			}
		}
		synchronized(responseLock){
			responseLock.notifyAll();
		}
	}
	
	// simulate server down
	public void serverDown(){
		isDown = true;
		requests.clear();
		responses.clear();
		servers.clear();
		synchronized(responseLock){
			responseLock.notify();
		}
	}
	
	// turn off server down flag
	public void serverUp(){
		isDown = false;
	}
	
	// add recovered jvm back to the list
	public void serverUpNotify(String jvm){
		int pid = servers.isEmpty() ? 1 : servers.last().id + 1;
		servers.add(new JVMServer(jvm, pid));
	}

	private void bubble(String s){
        bubble(s,0);
    }
	private void bubble(String s, int level){
	    StackTraceElement[] elements = Thread.currentThread().getStackTrace();
	    StackTraceElement element = elements[elements.length- 2];
		String place = element.getClassName() + "." + element.getMethodName() + "():" + element.getLineNumber();
		String indent = level <= 0 ? "" : String.format("% + level*2", " ");
        String log = String.format("@%-11s: ", mMyJVM + " (" + mMyBranch +"-CM)") + indent + s + " in " + place;
        System.out.println(log);
        try { 
        	logWriter.write(log + "\n");
        	logWriter.flush();
		} catch (IOException e) {}
    }

}