import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.LinkedBlockingQueue;

// this class represents a single branch running on a JVM
// (act as one of the servers in Primary/Backup protocol)
// it also manages ClientModule responsible for connecting to neighbor branches
// (act as a client in Primary/Backup protocol)
class ServerModule extends Thread{
	AccountManager 			   accountManager;
	ConcurrentSkipListSet<JVMServer>       servers;  // server replicas (sorted by process id, that is primary is at the head)
	ConcurrentHashMap<String,HashSet<Msg>> responses  = new ConcurrentHashMap<String,HashSet<Msg>>();  // collect responses from replicas
	LinkedBlockingQueue<Msg>   requests      = new LinkedBlockingQueue<Msg>();  // received BankMessage queue
	private final Object       responseLock  = new Object();   // to implement wait receive or failure
	int                        runningNumber = 0;              // used in conjuction with message hashcode to generate UID
	int                        responseCount = 0;
	
	BranchServer mMyServer 	= null;
	String  mMyJVM 			= null;
	String  mMyBranch 		= null;
	Channel mChannel		= null;
	Config  mConfig         = null;
	boolean isDown          = false;

	// for recovery
	String  mSyncJVM            = null;                 // name of JVM that is just recovered
	HashSet<PBMsg> mSyncNewMsgs = new HashSet<PBMsg>(); // collect state from current primary/backup servers
	                                                    // we want primary's state but just in case primary failed...
	
	// hashtable of Client module for neighbor branches ( Branch->ClientModule )
	HashMap<String,ClientModule> mClientModules = new HashMap<String,ClientModule>();
	
	// for debug
	FileWriter logWriter;
	
	public ServerModule(BranchServer server, Config c, String jvm, String branch,TreeSet<JVMServer> servers){
		try {logWriter = new FileWriter("log/" + jvm + "(" + branch + "-SM).txt");
		} catch (IOException e) {e.printStackTrace();}
		
		mMyJVM    = jvm;
		mMyBranch = branch;
		mMyServer = server;
		mChannel  = server.mChannel;
		mConfig   = c;
		accountManager = new AccountManager(branch);
		this.servers = new ConcurrentSkipListSet<JVMServer>(servers);
		
		// create ClientModule for each branch that this branch can interact with 
		// (specifically for transfer operaiton)
		for( String neighbor_branch : mConfig.mNeighborBranches.get(branch) ){
			mClientModules.put(neighbor_branch, new ClientModule(   // create ClientModule
				mChannel, jvm, neighbor_branch,                     // given housekeeping info
				mConfig.mBranchServers.get(neighbor_branch)));      // and servers info (branch_file.txt)
		}
		this.dumpConfiguration();
		this.start(); // start ServerModule message loop
	}
	
	public void run(){
		while(true){
			Msg   m  = null;
			BMsg  bm = null;
			PBMsg pm = null;
			FDMsg fm = null;
			try { 
				bubble("Waiting for new message");
				m = requests.take();
				bubble("Message Loop Received: " + m);
				if(isDown) {
					bubble("Down: ignoring " + m);      // ignore incoming message to simulate failure
					continue;
				}
				switch(m.type){
				case BMsg.DEPOSIT:
				case BMsg.WITHDRAW:
				case BMsg.TRANSFER:
				case BMsg.IB_TRANSFER:
				case BMsg.QUERY:
					bm = new BMsg((BMsg)m);
					this.receiveUpdateHist(bm);            // bank operations
					this.sendSyncRequest();                // send sync to backups
					if(isDown) continue;                   // if failed, stop sending response
					this.sendUpdateHistResponse(bm);       // update status/sync
					break;
				case PBMsg.SYNC_REQUEST:
					pm = new PBMsg((PBMsg)m);
					this.receiveSyncRequest(pm);     // backups received sync request
					break;
				case FDMsg.SERVER_UP_NOTIFY:
					fm = new FDMsg((FDMsg)m);
					this.serverUpNotify(fm.jvm_param);     // inform client module of new servers
					this.sendSyncNewRequest(fm);           // all P/B noticed that someone recovered
					break;
				case PBMsg.SYNC_NEW_REQUEST:               // a new backup received sync new request
					pm = new PBMsg((PBMsg)m);
					this.receiveSyncNewRequest((PBMsg)m); 
					break;
				}
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			} 
		}		
	}
	
	// utility functions
	public boolean isPrimary(){ return !servers.isEmpty() && servers.first().name.equals(mMyJVM); }
	public void receiveRequest(Msg m){requests.offer(m);}  // make request FIFO
	public void receiveResponse(Msg m){                    // manage responses set (hashed by source jvm)
		if(!responses.containsKey(m.srcJVM)) responses.put(m.srcJVM, new HashSet<Msg>());
		synchronized(responseLock){
			this.responseCount++;
			responses.get(m.srcJVM).add(m);
			responseLock.notifyAll();  // wake up threads that are waiting for response to check new response
		}
	}

	// process business logic
	// return true if we need to sync after this updateHist operation
	// ==============================================================
	// P/B Protocol: EVENT receive("updateHist",(uid,op)) from client
	//                            serialNumber,type    jvm_sender
	// ==============================================================
	private boolean receiveUpdateHist(BMsg bm){
		bubble("receiveUpdateHist()");
		try { 
			// ==============================================================
			// P/B Protocol: remove server p < myself
			//               check serialNumber implemented in accountManager
			// ==============================================================
			while(!isPrimary()) {
				bubble("servers size=" + servers.size() + " head=" + servers.first());
				servers.remove(servers.first());
			}
			
			switch(bm.type){
			case BMsg.DEPOSIT: // deposit command from GUI
				accountManager.deposit(bm.srcAccount, bm.serialNumber, bm.amount);
				return true;
		
			case BMsg.IB_TRANSFER: // inter/intra-bank transfer
				accountManager.deposit(bm.dstAccount, bm.serialNumber, bm.amount);
				return true;
				
			case BMsg.WITHDRAW: // withdraw command from GUI
				accountManager.withdraw(bm.srcAccount, bm.serialNumber, bm.amount);
				return true;
				
			case BMsg.TRANSFER: // transfer command from BUI
				// check source and destination
				String srcBranch = bm.srcAccount.substring(0,2);
				String dstBranch = bm.dstAccount.substring(0,2);
				if( !mClientModules.containsKey(dstBranch) ){
					bubble("Neighbor not found exception! " + dstBranch);
					throw new NeighborNotFoundException();
				}

				// create Msg for transfer operation
				BMsg transferMessage  = new BMsg();
				transferMessage.type 		 = BMsg.IB_TRANSFER;
				transferMessage.srcBC        = srcBranch;
				transferMessage.dstBC        = dstBranch;
				transferMessage.serialNumber = bm.serialNumber;
				transferMessage.srcAccount   = bm.srcAccount;
				transferMessage.dstAccount	 = bm.dstAccount;
				transferMessage.amount 		 = bm.amount;
				mClientModules.get(dstBranch).receiveRequest(transferMessage);

				// withdraw locally
				accountManager.withdraw(bm.srcAccount, bm.serialNumber, bm.amount);
				return true;
			case BMsg.QUERY:
				// will show the balance in the response
				break;
			default:
				System.out.println("Undefined Message " + bm.type);
			}
		} catch (DuplicateSerialNumberException dsne) {
			bubble("Duplicate Serial Number " + bm.serialNumber + ". " + bm.getOperation() + " cancelled");
		} catch (NotMyAccountException nmae) {
			bubble("Cannot perform operation on account managed by other branch (I'm " + 
								mMyBranch + ", requested " + bm.srcAccount);
		} catch (NeighborNotFoundException nnfe){
			bubble("Cannot connect to branch " + bm.dstBC);
		}
		return false;
	}

	// send sync to all p/b
	// ==============================================================
	// P/B Protocol: sync(H')
	// ==============================================================
	private void sendSyncRequest(){
		// sentServers.clear();
		// ==============================================================
		// P/B Protocol: uid2:=genUID()
		// ==============================================================
		PBMsg syncRequest = new PBMsg();
		syncRequest.type  = PBMsg.SYNC_REQUEST;
		syncRequest.state = accountManager;
		syncRequest.uid = accountManager.hashCode()+runningNumber++;
		syncRequest.srcBC = this.mMyBranch;
		syncRequest.dstBC = this.mMyBranch;
		// ==============================================================
		// P/B Protocol: send "syncRequest" to all servers
		// ==============================================================
		for(JVMServer server : servers){
			if( server.name.equals(this.mMyJVM)) continue; // don't send to myself
			try {
				bubble("send Sync Request to " + server.name);
				syncRequest.dstJVM = server.name;
				mChannel.sendToJVM(syncRequest);
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		this.waitResponse(syncRequest.uid); // wait for response from non-faulty processes
	}	

	// ==============================================================
	// P/B Protocol: EVENT receive("sync",(uid,op)) from primary
	// ==============================================================
	private void receiveSyncRequest(PBMsg pm){
		// ==============================================================
		// P/B Protocol: remove server p < primary
		// ==============================================================
		while(!servers.first().name.equals(pm.srcJVM)) servers.remove(servers.first());

		accountManager = pm.state;   // update state from primary
		
		// send("response",uid) to primary
		PBMsg syncResponse  = new PBMsg();
		syncResponse.type   = PBMsg.SYNC_RESPONSE;
		syncResponse.dstJVM = pm.srcJVM;
		syncResponse.srcBC  = this.mMyBranch;
		syncResponse.dstBC  = this.mMyBranch;
		syncResponse.uid    = pm.uid;
		
		try {
			bubble("send SyncResponse to " + pm.srcJVM);
			mChannel.sendToJVM((Msg)syncResponse);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	// receive all from non-fautly process
	private void waitResponse(int uid){
		boolean receivedAll = false;
		int lastResponseCount = -1;
		while(!receivedAll){
			receivedAll = true;             // suppose if we have already received all...then
			synchronized(responseLock){		// wait for response or server_down_notify event
				for(JVMServer server: servers){ // for each server
					if( server.name.equals(this.mMyJVM)) continue;
					boolean exists = false;     // there must exists a response
					if(responses.containsKey(server.name)) {
						for(Msg m: responses.get(server.name)) {
							if (m.uid == uid){  // with the desired UID
								exists = true;
								break;
							}
						}
					}
					if(!exists) {
						receivedAll = false;    // otherwise... back to wait
						break;
					}
				}
				if(receivedAll) break;
				bubble("Blocking in waitResponse (from all non-faulty) uid=" + uid); 
			
				try {
					if(lastResponseCount == this.responseCount) {
						responseLock.wait();
					}
					lastResponseCount = this.responseCount;
				} catch (InterruptedException e) {}
			}
		}
		bubble("waitResponse() completed for uid=" + uid);
	}

	// send response back to the sync request sender
	// ==========================================================
	// P/B Protocol: send("response",(uid,H)) to client
	//                       serialNumber,accountNo/Balance  jvm_sender
	// ==========================================================
	private void sendUpdateHistResponse(BMsg bm){
		// show relevant information on the console
		int balance = -1;
		try {
			if(bm.type == BMsg.IB_TRANSFER){
				balance = accountManager.query(bm.dstAccount);
				bubble(bm.dstAccount + "'s balance = " + String.format("%5d", balance) + 
					   " [" + bm.getOperation()+ " from " + bm.srcAccount + " S/N="+ bm.serialNumber +"]");
			} else { // show balance
				balance = accountManager.query(bm.srcAccount);
				bubble(bm.srcAccount + "'s balance = " + String.format("%5d", balance) + 
					   " [" + bm.getOperation()+ " S/N="+ bm.serialNumber +"]");
			}
		} catch (NotMyAccountException e1) {} // do nothing
		
		// response message to the client					
		BMsg responseToClient  		  = new BMsg();
		responseToClient.type 		  = bm.type==BMsg.IB_TRANSFER ?              // client is another BranchServer
										BMsg.IB_TRANSFER_OK : BMsg.SHOW_BALANCE; // client is BranchGUI
		responseToClient.dstJVM       = bm.srcJVM;
		responseToClient.uid          = bm.uid;
		responseToClient.srcBC        = bm.srcBC;
		responseToClient.dstBC        = bm.dstBC;
		responseToClient.srcAccount   = bm.srcAccount;
		responseToClient.dstAccount   = bm.dstAccount;
		responseToClient.amount 	  = balance;
		responseToClient.serialNumber = bm.serialNumber;
		
		try { // try to send response back to the client
			mChannel.sendToJVM(responseToClient);
			bubble("sent updateHistResponse to " + responseToClient.dstJVM + " | uid="+bm.uid);
		} catch (NeighborNotFoundException e) {
			bubble("could not send to " + responseToClient.dstJVM);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// remove the failed server from our servers list
	// and release wait-lock to recheck wait-receiving condition
	public void serverDownNotify(String jvm){
		if(isDown) return;
		bubble("serverDownNotify " + jvm + " size=" + servers.size());
		
		// delete all pending responses
		if(responses.containsKey(jvm)) responses.put(jvm, new HashSet<Msg>());
		
		// stop sync operation for new backup
		if(jvm.equals(mSyncJVM)) {         
			mSyncJVM = null;
			synchronized(responseLock){
				responseLock.notifyAll();
			}
		}
		
		// remove faulty server
		for(JVMServer server : servers){  
			if(server.name.equals(jvm)) { 
				bubble("removing " + server + " primary=" + servers.first().name);
				servers.remove(server);
				break;
			}
		}
		
		// propagate information to client module
		for(ClientModule clientModule : mClientModules.values()){
			clientModule.serverDownNotify(jvm);
		}
		
		// wake up threads that are waiting for responses from non-faulty servers
		// (there might be no more servers to wait for)
		synchronized(responseLock){
			responseLock.notifyAll();
		}
	}
	
	// simulated failure. delete all data structure
	// and cancel wait lock
	public void serverDown(){
		isDown = true;
		accountManager = new AccountManager(mMyBranch);
		requests.clear();
		responses.clear();
		servers.clear();
		servers.add(new JVMServer(mMyJVM,1)); // initialized
		mSyncJVM = null;
		
		// propagate failure to client modules
		for(ClientModule clientModule : mClientModules.values()){
			clientModule.serverDown();
		}
		
		synchronized(responseLock){
			responseLock.notify();
		}
	}
	
	// ==========================================================
	// when there is a server recovered
	// 1. update list of servers for clients by adding new backup
	// 2. send sync new request to the new backup
	// ==========================================================
	public void serverUpNotify(String recovered_jvm){
		// propagate information to client module
		for(String branch: mClientModules.keySet()){
			for(JVMServer jvm: mConfig.mBranchServers.get(branch)){
				if(jvm.name.equals(recovered_jvm)) {
					mClientModules.get(branch).serverUpNotify(recovered_jvm); // add to tail
					break;
				}
			}
		}		
	}
	
	// send state information so that a new server can join as a backup
	private void sendSyncNewRequest(FDMsg fm){
		if(fm.jvm_param.equals(mMyJVM)) return; // skip self (need info from current Primary/Backups)
		PBMsg syncnew  = new PBMsg();
		syncnew.type   = PBMsg.SYNC_NEW_REQUEST;
		syncnew.srcBC  = this.mMyBranch;
		syncnew.dstBC  = this.mMyBranch;
		syncnew.dstJVM = fm.jvm_param;        // send to the recovered jvm
		syncnew.state  = this.accountManager; // send the latest bank account status
		syncnew.servers= this.servers;        // servers state
		for(String branch : this.mClientModules.keySet()){                           // for each client modules
			syncnew.clients.put( branch , this.mClientModules.get(branch).servers ); // servers state
		}
		syncnew.uid    = syncnew.hashCode() + runningNumber++; // gen uid
		this.mSyncJVM  = fm.jvm_param;
		
		// send to mSyncJVM
		try {
			bubble("send SyncNew to " + fm.jvm_param);
			mChannel.sendToJVM(syncnew);
		} catch(Exception e){
			e.printStackTrace();
		}

		// wait for response from the new server (stop processing other requests to ensure state consistency)
		while(this.mSyncJVM!=null){
			try {
				bubble("waiting for reply from recovered jvm=" + mSyncJVM + " with uid=" + syncnew.uid);
				synchronized(responseLock){
					responseLock.wait();
				}
			} catch (InterruptedException e) {}
			if(isDown) return;
			if(responses.containsKey(mSyncJVM)) {
				for(Msg m: responses.get(mSyncJVM)){
					if(m.uid == syncnew.uid){  // (?,(uid,?)) in responses
						// should we clean up responses??
						bubble("received: " + m.uid );
						// attach the new backup to the end of the list
						servers.add(new JVMServer(mSyncJVM, servers.last().id+1));
						mSyncJVM = null;
						// mSyncNewMsgs.clear() if uid ==;
						bubble("DUMP FROM EXISTING P/B");
						this.dumpConfiguration();
						return;
					}
				}
			} 
		}
	}
	
	// a new server that will join as a backup receives state information
	private void receiveSyncNewRequest(PBMsg pm){
		this.mSyncNewMsgs.add(pm);
		
		// send("response",uid) to the sender
		PBMsg syncResponse  = new PBMsg();
		syncResponse.type   = PBMsg.SYNC_NEW_RESPONSE;
		syncResponse.dstJVM = pm.srcJVM;
		syncResponse.srcBC  = this.mMyBranch;
		syncResponse.dstBC  = this.mMyBranch;
		syncResponse.uid    = pm.uid;
			
		try {
			bubble("send SyncResponse");
			mChannel.sendToJVM((Msg)syncResponse);
		} catch(Exception e){
			e.printStackTrace();
		}
		
		checkSyncNewComplete(); // wait for primary's state information
	}
	
	private void checkSyncNewComplete(){
		if( mSyncNewMsgs.isEmpty() ) return;
		
		// wait until received from current primary
		for(PBMsg pm: mSyncNewMsgs){
			if(pm.servers.first().name.equals(pm.srcJVM)) {
				this.accountManager = pm.state;                             // update state
				this.servers = pm.servers;                                  // update servers
				servers.add(new JVMServer(mMyJVM, servers.last().id+1));    // attach my jvm
				bubble("SYNC SERVERS COMPLETED: primary=" + servers.first() + " size=" + servers.size());
				for(String branch : this.mClientModules.keySet()){                    // for each client modules
					this.mClientModules.get(branch).servers = pm.clients.get(branch); // update servers
					// bubble("SYNC CLIENTS (" + branch + ") COMPLETED: primary=" + mClientModules.get(branch).servers.first() + " size=" + mClientModules.get(branch).servers.size());
				}
				this.dumpConfiguration();
				return;
			}
		}
	}
		
	// turn server down flag off
	public void serverUp(){
		isDown = false;
		// propagate failure to client modules
		for(ClientModule clientModule : mClientModules.values()){
			clientModule.serverUp();
		}
	}
	
	private void bubble(String s){
        bubble(s,0);
    }
	
	private void bubble(String s, int level){
	    StackTraceElement[] elements = Thread.currentThread().getStackTrace();
	    StackTraceElement element = elements[elements.length- 2];
		String place = element.getClassName() + "." + element.getMethodName() + "():" + element.getLineNumber();
		String indent = level <= 0 ? "" : String.format("% + level*2", " ");
		String log = String.format("@%-11s: ", mMyJVM + " (" + mMyBranch +"-SM)") + indent + s + " in " + place;
        System.out.println(log);
        try { 
        	logWriter.write(log + "\n");
        	logWriter.flush();
		} catch (IOException e) {}
	}
	
	private void dumpConfiguration(){
		bubble("DUMP servers");
		for(JVMServer s : servers){
			bubble("- " + s);
		}
		for(String branch : mClientModules.keySet()){
			bubble("- Client branch" + branch);
			for(JVMServer s : mClientModules.get(branch).servers){
				bubble("- - " + s);
			}
		}
	}
}
