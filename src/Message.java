import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.ConcurrentSkipListSet;

// superclass of all messages
@SuppressWarnings("serial")
class Msg implements Serializable{
	public Msg(){}
	public Msg(Msg m){
		type   = m.type;
		srcJVM = m.srcJVM;
		dstJVM = m.dstJVM;
		srcBC  = m.srcBC;
		dstBC  = m.dstBC;
		uid    = m.uid;
	}
	public int    type;
	public String srcJVM;
	public String dstJVM;
	public String srcBC;
	public String dstBC;
	public int    uid;
	
	public String toString(){
		return "type=" + type + " src=" + srcJVM;
	}
}

// BankMessage (for bank operations)
@SuppressWarnings("serial")
class BMsg extends Msg{ // BankMessage
	public BMsg(){}
	public BMsg(BMsg bm){
		super(bm);
		serialNumber = bm.serialNumber;
		srcAccount   = bm.srcAccount;
		dstAccount   = bm.dstAccount;
		amount       = bm.amount;
	}
	
	// bank operation parameters
	public String 	serialNumber;      // unique serial number for each transaction
	public String 	srcAccount;        // (bb.aaaaa) used for deposit/withdraw/query
	public String	dstAccount;        // (bb.aaaaa) only used for transit
	public int		amount;            // can be negative for reply	
	
	public static final int DEPOSIT 	    =  1001;
	public static final int WITHDRAW	    =  1002;
	public static final int QUERY	     	=  1003;
	public static final int TRANSFER      	=  1004;
	public static final int SHOW_BALANCE	=  1005;
	public static final int IB_TRANSFER   	=  1006; // inter-bank transfer
	public static final int IB_TRANSFER_OK  =  1007; // inter-bank transfer response
	public static final int SEND_FAILED     =  1008;
	
	public String getOperation(){
		switch(type) {
		case DEPOSIT:  		return "Deposit";
		case WITHDRAW: 		return "Withdraw";
		case QUERY:    		return "Query";
		case TRANSFER: 		return "Transfer (-)";
		case SHOW_BALANCE: 	return "Show Balance";
		case IB_TRANSFER: 	return "Transfer (+)";
		case IB_TRANSFER_OK:return "Transfer OK";
		case SEND_FAILED:   return "Send Failed";
		}
		return "Undefined";
	} 
	
	public String toString(){
		StringBuffer sb=new StringBuffer();
		switch(type){
		case DEPOSIT:    sb.append("Deposit $"  + amount + " into A/C " + srcAccount); break;
		case WITHDRAW:   sb.append("Withdraw $" + amount + " from A/C " + srcAccount); break;
		case QUERY:      sb.append("Query A/C " + srcAccount); break;
		case TRANSFER:   sb.append("Transfer $" + amount + " from A/C " + srcAccount + " to " + dstAccount); break;
		case IB_TRANSFER:sb.append("Transfer $" + amount + " from A/C " + srcAccount + " to " + dstAccount); break;
		default: sb.append(getOperation());
		}
		return sb.toString();
	}
}

// Primary/Backup protocol message
@SuppressWarnings("serial")
class PBMsg extends Msg{                             // Primary/Backup Protocol Message
	public PBMsg(){}
	public PBMsg(PBMsg m){
		super(m);
		state   = m.state;
		servers = m.servers;
		clients = m.clients;
	}
	
	public AccountManager state;                     // account state for syncs
	public ConcurrentSkipListSet<JVMServer> servers; // latest operating servers
	public HashMap<String,ConcurrentSkipListSet<JVMServer>> clients
		= new HashMap<String,ConcurrentSkipListSet<JVMServer>>(); // latest operating clients
	
	public static final int SYNC_REQUEST      = 2001;
	public static final int SYNC_RESPONSE     = 2002;
	public static final int SYNC_NEW_REQUEST  = 2011;
	public static final int SYNC_NEW_RESPONSE = 2012;
	
	public String getOperation(){
		switch(type) {
		case SYNC_REQUEST:  	return "Sync Request";
		case SYNC_RESPONSE: 	return "Sync Response";
		case SYNC_NEW_REQUEST:  return "Sync NEW Request";
		case SYNC_NEW_RESPONSE: return "Sync NEW Response";
		}
		return "Undefined";
	}
	public String toString(){ return getOperation(); }
}


// Failure Detection messages
@SuppressWarnings("serial")
class FDMsg extends Msg{ // Fault Detector Message
	public FDMsg(){}
	public FDMsg(FDMsg m){
		super(m);
		jvm_param      = m.jvm_param;
		jvm_dependency = m.jvm_dependency;
	}
	
	public String         jvm_param;         // a jvmname as a parameter
	public String[]       jvm_dependency;    // for register

	// for failure detection
	public static final int REGISTER           = 3001;
	public static final int SERVER_UP          = 3002;
	public static final int SERVER_DOWN        = 3003;
	public static final int SERVER_UP_NOTIFY   = 3004;
	public static final int SERVER_DOWN_NOTIFY = 3005;
}


// BranchGUI driver special messages
@SuppressWarnings("serial")
class DriverMsg extends Msg{ // Test Driver Message
	public String   jvm_param;         // a jvmname as a paramete
	public String 	serialNumber;      // unique serial number for each transaction
	public String 	srcAccount;        // (bb.aaaaa) used for deposit/withdraw/query
	public String	dstAccount;        // (bb.aaaaa) only used for transit
	public int		amount;            // can be negative for reply	
	
	public static final int DRIVER_DEPOSIT     = 4001;
	public static final int DRIVER_WITHDRAW    = 4002;
	public static final int DRIVER_TRANSFER    = 4003;
	public static final int DRIVER_QUERY       = 4004;
	public static final int DRIVER_DOWN        = 4005;
	public static final int DRIVER_UP          = 4006;
	public static final int DRIVER_DOWN_NOTIFY = 4007;
	public static final int DRIVER_UP_NOTIFY   = 4008;
	
}