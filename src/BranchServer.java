import java.util.HashMap;


public class BranchServer {
	Channel        mChannel;   				// communication channel wrapper class
	Config         mConfig;                 // wrapper for config files (topology, jvm, branch)
	HashMap<String,ServerModule> mServerModules = new HashMap<String,ServerModule>();
	private String mMyJVM;
	
	// begin here
	public BranchServer(String myJVM, String topofile, String jvmfile, String branchfile){
		mMyJVM   = myJVM;
		mConfig  = new Config(topofile, jvmfile, branchfile);
		mChannel = new Channel(myJVM,mConfig);
		
		// for each Branch that this JVM manages (either primary or backup)
		for(String branch: mConfig.mBranchServers.keySet()){
			ServerModule serverModule = new ServerModule(     // create a server module
					this, mConfig, myJVM, branch,             // given house keeping info
					mConfig.mBranchServers.get(branch)        // and intial primary/backup replica info (basically branch_file.txt)
				);
			mServerModules.put(branch,serverModule);
		}
		waitAndProcessMessage();
	}
	
	private void waitAndProcessMessage(){
		System.out.println("Entering message loop");
		while(true){
			try { 
				Msg m = null;
				// bubble("ready for new message");
				m = mChannel.receive();
				bubble("Message Loop Received: " + m);
				// Business Message
				if(m.getClass() == BMsg.class) {
					BMsg bm = (BMsg)m;
					switch(bm.type){
					case BMsg.DEPOSIT:
					case BMsg.WITHDRAW:
					case BMsg.TRANSFER:
					case BMsg.QUERY:
						dispatchRequest(bm.srcBC,m);  // dispatch request to source branch server module
						break;
					case BMsg.IB_TRANSFER:
						dispatchRequest(bm.dstBC,m);  // dispatch request to destination branch server module
						break;
					case BMsg.IB_TRANSFER_OK:
						dispatchResponse(bm.dstBC,m); // dispatch response to destination branch server module
						break;
					}
				// Primary/Backup Synchronization Messages
				} else if(m.getClass()==PBMsg.class) {
					PBMsg pm = (PBMsg)m;
					switch(pm.type){
					case PBMsg.SYNC_REQUEST:
					case PBMsg.SYNC_NEW_REQUEST:
						dispatchRequest(pm.srcBC,m);  // dispatch request to source branch server module
						break;
					case PBMsg.SYNC_RESPONSE:
					case PBMsg.SYNC_NEW_RESPONSE:
						dispatchResponse(pm.srcBC,m); // dispatch response to source branch server module
						break;
					}
				// Failure Detection Messages
				} else if(m.getClass()==FDMsg.class) {
					FDMsg fm = (FDMsg)m;
					switch(fm.type){
					case FDMsg.SERVER_DOWN:
						if(fm.jvm_param.equals(mMyJVM)){
							for(ServerModule serverModule : mServerModules.values()){
								serverModule.serverDown();
							}							
						}
						break;
					case FDMsg.SERVER_DOWN_NOTIFY:
						if(!fm.jvm_param.equals(mMyJVM)){
							for(ServerModule serverModule : mServerModules.values()){
								bubble("calling serverModule.serverDownNotify() with " + fm.jvm_param);
								serverModule.serverDownNotify(fm.jvm_param);
							}
						}
						break;
					case FDMsg.SERVER_UP:
						if(fm.jvm_param.equals(mMyJVM)){
							for(ServerModule serverModule : mServerModules.values()){
								serverModule.serverUp();
							}							
						}	
						break;
					case FDMsg.SERVER_UP_NOTIFY:
						if(!fm.jvm_param.equals(mMyJVM)){
							for(String branch : mServerModules.keySet()){            // check if recovered jvm is needed by any branch
								for(JVMServer js : mConfig.mBranchServers.get(branch) ) {
									if(js.name.equals(fm.jvm_param)) {
										// bubble("process SERVER_UP_NOTIFY: " + branch + " " + js.name);
										mServerModules.get(branch).receiveRequest(fm);   // if so send a state to it
									}
								}
							}							
						}
						break;
					}
				}
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			} catch (NeighborNotFoundException nnfe){
				nnfe.printStackTrace();
			}
		}
	}
	
	// put message into the "responses" queue of server module of the branch
	private void dispatchResponse(String branch, Msg m) throws NeighborNotFoundException {
		bubble("dispatchResponse from branch " + branch + " type=" + m.type + " uid=" + m.uid);
		if(!mServerModules.containsKey(branch)) throw new NeighborNotFoundException();
		mServerModules.get(branch).receiveResponse(m);					
	}
	
	// put message into the "requests" queue of server module of the branch
	private void dispatchRequest(String branch, Msg m) throws NeighborNotFoundException {
		if(!mServerModules.containsKey(branch)) throw new NeighborNotFoundException();
		mServerModules.get(branch).receiveRequest(m);
	}
	
	private void bubble(String s){
        System.out.println(String.format("@%-11s: ",mMyJVM + " (Server)") + s);
    }

	public static void main(String[] args){
		new BranchServer(
				args.length > 0 ? args[0] : "S1",
				args.length > 1 ? args[1] : "topology_file.txt",
				args.length > 2 ? args[2] : "jvm_file.txt",
				args.length > 3 ? args[3] : "branch_file.txt"
		);
	}
}

